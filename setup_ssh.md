## Setting up account on remote with passwordless ssh from dev box
    
### Log into databike-prod for first time
    ssh {fistname}@162.243.143.135
    password = databike
    
    set you new password

### Get your local machine's id_rsa key

Start a new terminal session on your local machine
    
    $ cat ~/.ssh/id_rsa.pub
    
    // copy the output to the clip board
    
If nothing is printed to terminal do the following
    
    $ ssh-keygen -t rsa
    
    1. use the default file to save the key (/{user}/.ssh/id_rsa)
    2. leave passphrase empty for passwordless access

Rerun the cat comand above to get your rsa_key
    
    $ cat ~/.ssh/id_rsa.pub
    
    // copy the output to the clip board
    
### Setup your .ssh preference on the remote server

Switch back to your remote terminal session

    mkdir ~/.ssh
    chmod 700 ~/.ssh
    vi ~/.ssh/authorized_keys

    // paste in your rsa_key from local terminal session into your authorized_keys file save and close the file 

*(don't forget to press i for insert mode before pasting. Then to save and close press `Esc ` then `:x ` to save and close)*
