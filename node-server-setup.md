## initial setup
    Do Step #4 after for you local user account
    https://www.digitalocean.com/community/tutorials/initial-server-setup-with-centos-7

## node setup documentation *(Skip location setup for nginx)*
    https://www.digitalocean.com/community/tutorials/how-to-set-up-a-node-js-application-for-production-on-centos-7
    
## nginx setup for multiple domain mapping *(Do Section: Map a Domain To Service ... )*
    https://www.digitalocean.com/community/tutorials/how-to-host-multiple-node-js-applications-on-a-single-vps-with-nginx-forever-and-crontab

    