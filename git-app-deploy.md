## Deploy production and test apps with git

### Creating the project from scratch

1. Create project on local machine and check it into bitbucket with Source Tree

    ![](https://bytebucket.org/Databike/databike-docs/raw/a3093f6b23ba7ef61f752b90c3ea7bacdb035d61/images/st-1.png?token=96ec7980ff48e1423e638982fb76d1799d35ec1c)

    Make Sure that create remote branch is checked

    ![](https://bytebucket.org/Databike/databike-docs/raw/a3093f6b23ba7ef61f752b90c3ea7bacdb035d61/images/st-2.png?token=eb9c109c820bf274b08b6a2298d9e7ec113b2b69)

2. From the terminal cd to the project folder and make intial commit

        git add -A
        git commit -a
        (add commit message)
        git push origin master
        
3. Create the local test branch and create the remote test branch 

        git checkout -b test
        git push origin test

4. Create your dev branch 

        git checkout -b {dev-branch-name}
        git push origing {dev-branch-name}

*You now have local versions of the prod, test, branches as well as a dev branch.
All of them also have remote branches on bitbucket and can be deployed to a prod/test server*

### Checking out an existing project

1. Clone the repo in jetbrains app

    ![](https://bytebucket.org/Databike/databike-docs/raw/74f292f03824b116f121e1267f946e78d2ca9436/images/jb-1.png?token=f215e183b3df4dc56055f469b79e988e2dbff65f)
    
    
    ![](https://bytebucket.org/Databike/databike-docs/raw/74f292f03824b116f121e1267f946e78d2ca9436/images/jb-2.png?token=5d9ff602a0e0394c44324dbb64f70e0d784bf46f)

2. You should have the master branch locally

        git branch
        
        (should see master branch only)
        
3. Create a loacl test branch and force pull the origin/test branch

        git checkout -b test
        git pull -f origin test

4. Create you dev branch as above step 4.


## Create the git repos on the prod or test server

1. Login to the remote app server (see node-sever-setup.md for details) and switch to the node service account

        ssh {user}@ip.add.ress
        sudo su - node
        (enter sudo password)

2. Clone the master branch to a {project} directory inside the node SA $HOME dir
        
        git clone https://bitbucket.org/Databike/{repo-name}.git {project_name}
        
        (copy the link from the bitbucket site)

3. Clone the test branch to a {project-test} directory instde the node SA $HOME dir

        git clone -b test https://bitbucket.org/Databike/{repo-name}.git {project_name-test}

*There should be 2 directories in the node SA $HOME: 
`{project}` and `{project-test}`*

*If you `cd` into the respective directories and run the command `git branch` you will see the each git repo is only following a single branch `*master` in {project}
and `*test` in {project-test}*

4. Deploy changes to a prod repo (make sure you are using node SA)

        cd ~/{project}
        git pull origin master
        cd ..
        pm2 restart {project} / if {project} is already tracked by pm2 