## deploying a node apps with pm2 and nginx reverse proxy

1. Create master and test branches of the project in bitbucket


2. Login to you account on server and su to the node service account
    
        sudo su - node
    
3. Clone the master branch from url in bitbucket (leave off username)
        
        git clone https://bitbucket.org/Databike/cynk_io.git cynk

    *Output*
    
        Cloning into 'cynk'...
        Username for 'https://bitbucket.org': johnmdonich
        Password for 'https://johnmdonich@bitbucket.org': 
        remote: Counting objects: 20, done.
        remote: Compressing objects: 100% (20/20), done.
        remote: Total 20 (delta 6), reused 0 (delta 0)
        Unpacking objects: 100% (20/20), done.
        [node@databike-prod ~]$ ls
        cynk

4. If this is a node application and you have dependencies, make sure to run `npm install`.

5. Clone the test branch from the repo with differnt project-test name

        git clone -b test https://bitbucket.org/Databike/cynk_io.git cynk-test

    *Output*
    
        Cloning into 'cynk-test'...
        Username for 'https://bitbucket.org': johnmdonich
        Password for 'https://johnmdonich@bitbucket.org': 
        remote: Counting objects: 20, done.
        remote: Compressing objects: 100% (20/20), done.
        remote: Total 20 (delta 6), reused 0 (delta 0)
        Unpacking objects: 100% (20/20), done.
        [node@databike-prod ~]$ ls
        cynk  cynk-test
        
6. Start the node services with pm2 (should be listening on separate ports)

        pm2 start cynk/cynk.js --name="cynk"
        pm2 start cynk-test/cynk.js --name="cynk-test"
        
    *Output*
    
    ![](https://bytebucket.org/Databike/databike-docs/raw/74f292f03824b116f121e1267f946e78d2ca9436/images/pm2-output.png?token=b74751470d4bfd063f2e430f1d45e1ea6d84ea17)

7. Exit the node service account

        $ exit

8. Setup reverse nginx proxy server server block in `/etc/nginx/conf.d` directory *(file must end with .conf)*

        
        sudo vi /etc/nginx/conf.d/{domain}.conf

    **Update the domain and port that you node app is listening on**
    
    *prod file structure*
        
        server {
            listen 80;
        
            server_name {domain} www.{domain};
        
            location / {
                proxy_pass http://127.0.0.1:{port};
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection 'upgrade';
                proxy_set_header Host $host;
                proxy_cache_bypass $http_upgrade;
            }
        
            error_page 404 /404.html;
                location = /40x.html {
            }
        
            error_page 500 502 503 504 /50x.html;
                location = /50x.html {
            }
        
        }

    *setup test subdomain block*
        
        sudo vi /etc/nginx/conf.d/test.{domain}.conf

    *test file structure*
    
        server {
            listen 80;
        
            server_name test.{domain};
        
            location / {
                proxy_pass http://127.0.0.1:{port};
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection 'upgrade';
                proxy_set_header Host $host;
                proxy_cache_bypass $http_upgrade;
            }
        
            error_page 404 /404.html;
                location = /40x.html {
            }
        
            error_page 500 502 503 504 /50x.html;
                location = /50x.html {
            }
        
        }

9. restart the nginx webserver

        sudo systemctl restart nginx

10. Update the a dns records on namecheap.com according to the following example

    ![](https://bytebucket.org/Databike/databike-docs/raw/74f292f03824b116f121e1267f946e78d2ca9436/images/namecheap-dns-records.png?token=24e50192c412afbb8cac8988b62ab348b8bd30f5)
    